all: shell

test:
	singularity exec -B ~/Desktop/projects/patdb:/opt/patdb env.dir cd /opt/patdb/patdb/dl && /opt/pyenv/bin/python test.py

shell:
	singularity shell --writable -B ~/Desktop/projects/patdb:/opt/patdb env.dir /bin/bash

send: send-gpuCMM send-news-vm2

send-%:
	cp env.sif /mnt/$*

build: env.dir

pkg: env.sif

init:
	$(MAKE) -C lib/sampen

env.sif:
	sudo singularity build $@ env.dir

env.dir: thecontainer.def
	sudo singularity build --sandbox $@ $<

clean:
	sudo rm -rf env.sif env.dir


