
# Singularity patdb compute environment
intro to singularity [here](https://carpentries-incubator.github.io/singularity-introduction/)

# Pre requisite

- Compile sample entropy calculation code, which should be placed in `lib/sampen/`:
```bash
make init
```

- The folder containing the postgres secrets `psql`
- Right to `sudo`
- Definition file `thecontainer.def`

# Build environmment
1. Create sandbox environment:
```bash
make build
```
The sandbox environment is created in the `env.dir/` directory.

2. Create stand alone environment from the sandbox:
```bash
make pkg
```
The stand alone environment is created as a .sif file: `env.sif`.

# Use environment (patdb project specific)
Suppose that on the host, 
- the environment .sif file is `~/shared/env.sif`
- the project root is `~/projects/patdb`
- the code is located in `<project_root>/patdb/dl`

Add these lines to ~/.bash_aliases:
```bash
alias singularity-patdb-dl-make='singularity exec -B ~/projects/patdb:/opt/patdb --contain --nv ~/shared/env.sif make -C /opt/patdb/patdb/dl'
alias singularity-patdb-dl-python='singularity exec -B ~/projects/patdb:/opt/patdb --contain --nv ~/shared/env.sif /opt/pyenv/bin/python'
```

the `--nv` flag activates the nvidia cuda drivers

Source the aliases file:
```bash
. ~/.bash_aliases
```

To run a make recipe dry-run:
```bash
singularity-patdb-dl-make test -n
```

Get a python shell:
```bash
$ singularity-patdb-dl-python
Python 3.7.3 (default, Jul 25 2020, 13:03:44) 
[GCC 8.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import os 
>>> os.getcwd()
...
```


